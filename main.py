"""My sudoku implementation."""
from typing import TextIO

import checker as c
import tech as t
from board import Board
from ui import CLI
from value import V


def load_board() -> list[V]:
    import sys
    from pathlib import Path

    inf: str | TextIO
    if len(sys.argv) > 1:
        file = sys.argv[1]
        if not Path(file).is_file():
            print(f'File "{file}" not found!')
            sys.exit(1)
        with open(file) as f:
            return Board.parse("".join(f))
    else:
        print("please enter the sudoku board, and press ctrl-d, when you're done")
        with sys.stdin as f:
            return Board.parse("".join(f))


def main():
    board: list[V] = load_board()
    checkers: list[c.Checker] = [
        c.RowUniqueChecker(),
        c.ColUniqueChecker(),
        c.BoxUniqueChecker(),
    ]
    methods: list[t.Tech] = [
        t.EliminateOpts(),
        t.NakedSingle(),
        t.HiddenSingle(),
        t.LockedCandidates(),
        t.NakedPair(),
        t.HiddenPair(),
        t.NakedTripple(),
        t.HiddenTripple(),
        t.NakedQuad(),
        t.HiddenQuad(),
        t.XWing(),
        t.Swordfish(),
    ]
    cli = CLI(Board(board), checkers, methods)
    cli.loop()


if __name__ == "__main__":

    main()
