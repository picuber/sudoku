from abc import ABC, abstractmethod

from board import Board, Unit
from value import V


class Checker(ABC):
    @abstractmethod
    def check(self, board: Board) -> bool:
        pass

    def unitUniqueNums(self, cells: Unit) -> bool:
        cells = [cell for cell in cells if cell.val in V.nums()]
        return len(set(cells)) == len(cells)


class UniqueChecker(Checker):
    def check(self, board: Board) -> bool:
        return all([self.unitUniqueNums(unit) for unit in board.units])


class RowUniqueChecker(Checker):
    def check(self, board: Board) -> bool:
        return all([self.unitUniqueNums(row) for row in board.rows])


class ColUniqueChecker(Checker):
    def check(self, board: Board) -> bool:
        return all([self.unitUniqueNums(col) for col in board.cols])


class BoxUniqueChecker(Checker):
    def check(self, board: Board) -> bool:
        return all([self.unitUniqueNums(box) for box in board.boxes])
