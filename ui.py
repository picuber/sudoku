import checker as c
import tech as t
from board import Board, Key, Opts
from value import V


class CLI:
    def __init__(
        self,
        board: Board,
        checkers: list[c.Checker] = list(),
        techs: list[t.Tech] = list(),
    ):
        self.board: Board = board
        self.checkers: list[c.Checker] = checkers
        self.techs: list[t.Tech] = techs
        self.mode = True

    @staticmethod
    def write(line: str):
        print(line)

    @staticmethod
    def read() -> list[str] | None:
        try:
            return input("sudoku> ").split()
        except KeyboardInterrupt:
            return None

    def print_board(self) -> None:
        print(self.board, end="")

    @staticmethod
    def _parse_key(key_str: str) -> Key | None:
        key = key_str.split(",")
        if len(key) == 1:
            return int(key[0])
        elif len(key) == 2:
            return (int(key[0]), int(key[1]))
        else:
            return None

    @staticmethod
    def _parse_level(level_str: str) -> int | None:
        try:
            return int(level_str)
        except ValueError:
            return None

    @staticmethod
    def _options_str(opts: Opts):
        options = ", ".join(map(str, sorted(opts))) if len(opts) > 0 else "None"
        return f"options: {options}"

    def set_value(self, key: Key, v: V):
        self.board[key].val = v
        self.print_board()

    def get_options(self, key: Key):
        self.write(self._options_str(self.board[key].opts))

    def solved(self) -> bool:
        return self.board.solved

    def check(self) -> bool:
        return all(checker.check(self.board) for checker in self.checkers)

    def auto(self, level: int = 0, steps: int | None = 1) -> int:
        """If steps is None, run infinitely."""
        def run_techs(level: int = 0) -> bool:
            return any(
                tech.run(self.board) for tech in self.techs if tech.level() <= int(level)
            )

        count: int = 0
        while steps is None or (count < steps and run_techs(level)):
            count += 1

        return count

    def loop(self):
        while (line := self.read()) is not None:
            if not line:
                continue

            match line[0]:
                case "q" | "quit":
                    break

                case "h" | "help":
                    self.write("\t(h|help)")
                    self.write("\t(q|quit)")
                    self.write("\t(s|set) <row,col|idx> <value>")
                    self.write("\t(o|options) <row,col|idx> <col>")
                    self.write("\t(p|print)")
                    self.write("\t(?|solved)")
                    self.write("\t(c|check)")
                    self.write("\t(n|next)")
                    self.write("\t(a|auto)")

                case "s" | "set":
                    if len(line) < 3:
                        self.write(
                            f"Missing arguments: {line[0]} <row,col|idx> <value>"
                        )
                        continue

                    if (key := self._parse_key(line[1])) and (val := line[2]):
                        self.set_value(key, val)
                    else:
                        self.write("Arguments have wrong format!")

                case "o" | "options":
                    if len(line) < 2:
                        self.write(f"Missing arguments: {line[0]} <row,col|idx>")
                        continue

                    if key := self._parse_key(line[1]):
                        self.get_options(key)
                    else:
                        self.write("Arguments have wrong format!")

                case "p" | "print":
                    self.print_board()

                case "?" | "solved":
                    if self.solved():
                        self.write("Puzzle solved!")
                    else:
                        self.write("Not solved!")

                case "c" | "check":
                    if self.check():
                        self.write("All checks passed")
                    else:
                        self.write("The board has mistakes")

                case "n" | "next":
                    if level := self._parse_level(line[1]):
                        if self.auto(level, steps=1):
                            self.print_board()
                        else:
                            self.write(f"No step possible at level {level}")
                    else:
                        self.write("Wrong argument! Please enter a number.")

                case "a" | "auto":
                    if level := self._parse_level(line[1]):
                        if self.auto(level, steps=None):
                            if self.solved():
                                self.write("Solved!")
                            self.print_board()
                        else:
                            self.write(f"Auto at level {level} could not do anything")
                    else:
                        self.write("Wrong argument! Please enter a number.")

                case _:
                    self.write("Command Unknown")
