from abc import ABC, abstractmethod

import checker as c
import tech as t
from board import Board
from value import V


class Solver(ABC):
    def __init__(self):
        self.board: Board
        self.options: list[set[V]]
        self.checkers: list[c.Checker]
        self.techs: list[t.Tech]
        ...

    @abstractmethod
    @staticmethod
    def solve(self):
        pass
