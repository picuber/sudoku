from enum import IntEnum


class V(IntEnum):
    """A Value for a Sudoku field."""

    EMPTY = 0
    ONE = 1
    TWO = 2
    THREE = 3
    FOUR = 4
    FIVE = 5
    SIX = 6
    SEVEN = 7
    EIGHT = 8
    NINE = 9

    def __repr__(self):
        return "." if self is self.EMPTY else str(self.value)

    def __str__(self):
        return self.__repr__()

    @classmethod
    def nums(cls) -> set["V"]:
        return {
            cls.ONE,
            cls.TWO,
            cls.THREE,
            cls.FOUR,
            cls.FIVE,
            cls.SIX,
            cls.SEVEN,
            cls.EIGHT,
            cls.NINE,
        }

    @classmethod
    def valid(cls, c: str) -> bool:
        return c in "123456789.0"

    @classmethod
    def from_str(cls, string: str) -> "V" | None:
        if len(string) != 1:
            return None
        if string == ".":
            return cls.EMPTY
        elif string in "0123456789":
            return cls(int(string))
        else:
            return None
