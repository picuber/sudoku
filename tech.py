from abc import ABC, abstractmethod

from board import Board, Cell
from value import V


class Tech(ABC):
    @abstractmethod
    def level(self) -> int:
        pass

    @abstractmethod
    def run(self, board: Board) -> bool:
        pass

    @staticmethod
    def elimOpts(board: Board) -> bool:
        ret = False
        for cell in board:
            for target in board.union_of(cell.idx):
                if cell.val in target.opts:
                    ret = True
                    target.opts.remove(cell.val)
        return ret


class EliminateOpts(Tech):
    def level(self) -> int:
        return 0

    def run(self, board: Board) -> bool:
        return self.elimOpts(board)


class NakedSingle(Tech):
    def level(self) -> int:
        return 1

    def run(self, board: Board) -> bool:
        for cell in board:
            if len(cell.opts) == 1:
                cell.val = cell.opts.pop()
                self.elimOpts(board)
                return True
        return False


class HiddenSingle(Tech):
    def level(self) -> int:
        return 1

    def run(self, board: Board) -> bool:
        for unit in board.units:
            for num in V.nums():
                count: set[Cell] = set()
                for cell in unit:
                    if num in cell.opts:
                        count.add(cell)
                if len(count) == 1:
                    count.pop().val = num
                    self.elimOpts(board)
                    return True
        return False


class LockedCandidates(Tech):
    def level(self) -> int:
        return 2

    def run(self, board: Board) -> bool:
        return False


class NakedPair(Tech):
    def level(self) -> int:
        return 2

    def run(self, board: Board) -> bool:
        return False


class HiddenPair(Tech):
    def level(self) -> int:
        return 2

    def run(self, board: Board) -> bool:
        return False


class NakedTripple(Tech):
    def level(self) -> int:
        return 2

    def run(self, board: Board) -> bool:
        return False


class HiddenTripple(Tech):
    def level(self) -> int:
        return 2

    def run(self, board: Board) -> bool:
        return False


class NakedQuad(Tech):
    def level(self) -> int:
        return 2

    def run(self, board: Board) -> bool:
        return False


class HiddenQuad(Tech):
    def level(self) -> int:
        return 2

    def run(self, board: Board) -> bool:
        return False


class XWing(Tech):
    def level(self) -> int:
        return 3

    def run(self, board: Board) -> bool:
        return False


class Swordfish(Tech):
    def level(self) -> int:
        return 4

    def run(self, board: Board) -> bool:
        return False
