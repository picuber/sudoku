from typing import TypeAlias, Union

from value import V

Opts: TypeAlias = set[V]
Key: TypeAlias = Union[int, tuple[int, int]]


class Cell:
    def __init__(self, idx: int, val: V):
        if idx < 0 or idx >= 81 or not isinstance(val, V):
            raise ValueError
        self.idx = idx
        self._val: V = val
        self.opts: Opts = V.nums() if val is V.EMPTY else set()

    def __str__(self):
        return str(self._val)

    def __repr__(self):
        return f"Cell[{self.idx}]: {self._val} ({self.opts})"

    @property
    def val(self):
        return self._val

    @val.setter
    def val(self, value: V):
        if value is V.EMPTY:
            self._val = value
            self.opts = V.nums()
        else:
            self._val = value
            self.opts = set()


Unit: TypeAlias = list[Cell]  # Unit is {Box, Row, Col}


class Board:
    """Sudoku Board.
    We use 0 based indexing for Fields, Boxes, Rows and Columns"""

    def __init__(self, values: list[V] = list()):
        self.cells = [Cell(idx, val) for idx, val in enumerate(values)]
        if len(values) != 81:
            raise ValueError

    def __str__(self):
        def _row_str(r):
            out = f"{self.cells[r]}{self.cells[r+1]}{self.cells[r+2]}|"
            out += f"{self.cells[r+3]}{self.cells[r+4]}{self.cells[r+5]}|"
            return out + f"{self.cells[r+6]}{self.cells[r+7]}{self.cells[r+8]}\n"

        out = ""
        for r in range(9):
            if r == 3 or r == 6:
                out += "---+---+---\n"
            out += _row_str(r * 9)
        return out

    @staticmethod
    def parse(sudoku_str: str) -> list[V]:
        out = list()
        for c in sudoku_str:
            if V.valid(c):
                out.append(V.from_str(c))
                if len(out) == 81:
                    return out
        raise ValueError

    @staticmethod
    def _to_idx(key: Key) -> int:
        """Convert the coordinate access (row and column) to index access, and type check.
        If it is already index access, only tpye check."""

        if isinstance(key, int):
            if key < 0 or key >= 81:
                raise IndexError
            return key
        elif isinstance(key, tuple):
            if not (
                len(key) == 2 and isinstance(key[0], int) and isinstance(key[1], int)
            ):
                raise TypeError
            r, c = key
            if not (0 <= r <= 8 and 0 <= c <= 8):
                raise IndexError
            return r * 9 + c
        else:
            raise TypeError

    def __getitem__(self, key: Key) -> Cell:
        return self.cells[self._to_idx(key)]

    def __setitem__(self, key: Key, value: Cell):
        self.cells[self._to_idx(key)] = value

    def __iter__(self):
        return self.cells.__iter__()

    def box(self, br: int, bc: int) -> Unit:
        if not (0 <= br <= 2 and 0 <= bc <= 2):
            raise IndexError
        return [
            cell
            for i, cell in enumerate(self.cells)
            if i // 27 == br and (i % 9) // 3 == bc
        ]

    def box_of(self, key: Key) -> Unit:
        idx = self._to_idx(key)
        return self.box(idx // 27, (idx % 9) // 3)

    @property
    def boxes(self) -> list[Unit]:
        return [self.box(br, bc) for br in range(2) for bc in range(2)]

    def row(self, r: int) -> Unit:
        return [cell for i, cell in enumerate(self.cells) if i // 9 == r]

    def row_of(self, key: Key) -> Unit:
        return self.row(self._to_idx(key) // 9)

    @property
    def rows(self) -> list[Unit]:
        return [self.row(r) for r in range(9)]

    def col(self, c: int) -> Unit:
        return [cell for i, cell in enumerate(self.cells) if i % 9 == c]

    def col_of(self, key: Key) -> Unit:
        return self.col(self._to_idx(key) % 9)

    @property
    def cols(self) -> list[Unit]:
        return [self.col(c) for c in range(9)]

    @property
    def units(self) -> list[Unit]:
        return self.boxes + self.rows + self.cols

    def units_of(self, key: Key) -> list[Unit]:
        return [self.box_of(key), self.row_of(key), self.col_of(key)]

    def union_of(self, key: Key) -> set[Cell]:
        return {cell for unit in self.units_of(key) for cell in unit}

    @property
    def solved(self) -> bool:
        return all(set(map(lambda cell: cell.val, unit)) == V.nums() for unit in self.units)
